package shapes2d;
import java.lang.Math;
public class Circle {

	protected int radius;
	//public Circle() {
		//this.radius = 1;
	//}
	public Circle(int radius) {
		this.radius = radius;
		
	}
	public double area() {
		return Math.PI*radius*radius;
		
	}
	public String toString() {
		return"radius = "+ radius;
	}
}
