package lab;

public class Cylinder {

	private Circle c;
	private Rectangle r;
	public Cylinder(double radius, double height) {
		c =new Circle(radius) ;
		r = new Rectangle(c.Perimeter(),height);
		
	}
	public  double area() {
		return 2 *c.Area()+r.area();
	}
	public double volume() {
		return c.Area()*r.getLength();
	}
	


}
